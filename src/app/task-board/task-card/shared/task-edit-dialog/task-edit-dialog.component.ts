import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Task } from 'src/app/shared/models';
import { TaskBoardService } from 'src/app/task-board/task-board.service';


@Component({
  selector: 'app-task-edit-dialog',
  templateUrl: './task-edit-dialog.component.html',
  styleUrls: ['./task-edit-dialog.component.scss']
})
export class TaskEditDialogComponent implements OnInit {

  formGroup: FormGroup;

  readonly TASK_STATUSES = [
    {
      code: 'todo',
      name: 'TO DO'
    },
    {
      code: 'in_progress',
      name: 'IN PROGRESS'
    },
    {
      code: 'test',
      name: 'TEST'
    },
    {
      code: 'done',
      name: 'DONE'
    },
  ];

  constructor(
    private fb: FormBuilder,
    private taskBoardService: TaskBoardService,
    private dialogRef: MatDialogRef<TaskEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public task?: Task) { }

  ngOnInit() {
    this.formGroup = this.fb.group({
      title: [this.task ? this.task.title : '', [Validators.required]],
      description: [this.task ? this.task.description : ''],
      complexity: [this.task ? this.task.complexity : null, [Validators.required]],
      status: [this.task ? this.task.status : 'todo', [Validators.required]],
      estimatedTime: [this.task ? this.taskBoardService.secondsToHours(this.task.estimatedTime) : ''],
      actualTime: [this.task ? this.taskBoardService.secondsToHours(this.task.actualTime) : ''],
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    const task: Task = {
      id: this.task ? this.task.id : null,
      index: this.task ? this.task.index : -1,
      ...this.formGroup.value,
      estimatedTime: this.taskBoardService.hoursToSeconds(Number(this.formGroup.get('estimatedTime').value)),
      actualTime: this.taskBoardService.hoursToSeconds(Number(this.formGroup.get('actualTime').value)),
    };

    (this.task ?
      this.taskBoardService.updateTask(task) :
      this.taskBoardService.createTask(task)
    ).subscribe(() => this.dialogRef.close());
  }

}
