import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/shared/models';
import { MatDialog } from '@angular/material/dialog';
import { TaskEditDialogComponent } from './shared/task-edit-dialog/task-edit-dialog.component';

@Component({
  selector: 'app-task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss']
})
export class TaskCardComponent implements OnInit {
  @Input() task: Task;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  edit() {
    this.dialog.open(TaskEditDialogComponent, {
      data: this.task
    });
  }

}
