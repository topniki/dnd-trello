import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { TaskBoardRoutingModule } from './task-board-routing.module';
import { TaskBoardComponent } from './task-board.component';
import { TaskCardComponent } from './task-card/task-card.component';
import { TaskEditDialogComponent } from './task-card/shared/task-edit-dialog/task-edit-dialog.component';
import { BurndownChartModule } from '../shared/burndown-chart/burndown-chart.module';

@NgModule({
  declarations: [TaskBoardComponent, TaskCardComponent, TaskEditDialogComponent],
  imports: [
    CommonModule,
    TaskBoardRoutingModule,
    DragDropModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    BurndownChartModule
  ],
  entryComponents: [
    TaskEditDialogComponent
  ]
})
export class TaskBoardModule { }
