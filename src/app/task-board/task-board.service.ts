import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map, tap, filter } from 'rxjs/operators';
import { Project, Task, UpdateBoardPayload } from '../shared/models';

@Injectable({
  providedIn: 'root'
})
export class TaskBoardService {

  private project$: BehaviorSubject<Project> = new BehaviorSubject(null);

  get todoTasks$(): Observable<Task[]> {
    return this.project$.asObservable().pipe(
      filter(res => !!res),
      map(project => project.tasks.filter(t => t.status === 'todo').sort((a, b) => a.index - b.index))
    );
  }

  get inprogressTasks$(): Observable<Task[]> {
    return this.project$.asObservable().pipe(
      filter(res => !!res),
      map(project => project.tasks.filter(t => t.status === 'in_progress').sort((a, b) => a.index - b.index))
    );
  }

  get testTasks$(): Observable<Task[]> {
    return this.project$.asObservable().pipe(
      filter(res => !!res),
      map(project => project.tasks.filter(t => t.status === 'test').sort((a, b) => a.index - b.index))
    );
  }

  get doneTasks$(): Observable<Task[]> {
    return this.project$.asObservable().pipe(
      filter(res => !!res),
      map(project => project.tasks.filter(t => t.status === 'done').sort((a, b) => a.index - b.index))
    );
  }

  constructor(private http: HttpClient) { }

  getProjectById(id: string): Observable<Project> {
    return this.http.get<{ projects: Array<Project> }>('../../assets/json/mock-project.json', { params: { id } }).pipe(
      map(res => res.projects.find(p => p.id === id)),
      filter(res => !!res),
      tap(res => this.project$.next(res))
    );
  }

  createTask(newTask: Task): Observable<Task> {
    console.log('create newTask: ', newTask);
    return of(newTask).pipe(
      tap(task => {
        const tasks = this.project$.value.tasks;

        tasks.push(task);

        this.project$.next({
          ...this.project$.value,
          tasks
        });
      })
    );
  }

  updateTask(updatedTask: Task): Observable<Task> {
    console.log('update updatedTask: ', updatedTask);
    return of(updatedTask).pipe(
      tap(task => {
        const tasks = this.project$.value.tasks;

        tasks.splice(tasks.findIndex(t => t.id === task.id), 1, task);

        this.project$.next({
          ...this.project$.value,
          tasks
        });
      })
    );
  }

  updateBoard(payload: UpdateBoardPayload): Observable<void> {
    console.log(payload);
    return of(null);
  }

  hoursToSeconds(seconds: number): number {
    return Math.floor(seconds * 36000) / 10;
  }

  secondsToHours(hours: number): number {
    return Math.floor((hours / 3600) * 10) / 10;
  }
}
