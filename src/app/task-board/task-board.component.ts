import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { mergeMap, takeUntil } from 'rxjs/operators';

import { TaskBoardService } from './task-board.service';
import { TaskEditDialogComponent } from './task-card/shared/task-edit-dialog/task-edit-dialog.component';
import { Project, Task, TaskStatusType } from '../shared/models';
import { BurndownChartRealData } from '../shared/burndown-chart/shared';

@Component({
  selector: 'app-task-board',
  templateUrl: './task-board.component.html',
  styleUrls: ['./task-board.component.scss']
})
export class TaskBoardComponent implements OnInit {

  project: Project;

  todoTasks: Array<Task>;
  inprogressTasks: Array<Task>;
  testTasks: Array<Task>;
  doneTasks: Array<Task>;

  get fullEstimatedTime(): number {
    let fullTime = 0;
    this.project.tasks.forEach(t => fullTime += t.estimatedTime);
    return fullTime;
  }

  get actualTimesData(): Array<BurndownChartRealData> { // MOCK Chart data
    return [
      { date: new Date('07/01/2020'), value: 0 },
      { date: new Date('07/02/2020'), value: 180000 },
      { date: new Date('07/03/2020'), value: 252000 },
      { date: new Date('07/04/2020'), value: 540000 },
      { date: new Date('07/05/2020'), value: 680400 },
      { date: new Date('07/06/2020'), value: 802800 },
      { date: new Date('07/07/2020'), value: 1152000 },
      { date: new Date('07/08/2020'), value: 1152000 },
      { date: new Date('07/09/2020'), value: 1260000 },
      { date: new Date('07/10/2020'), value: 1364400 },
    ];
  }

  private destroy$: Subject<void> = new Subject<void>();

  constructor(private taskBoardService: TaskBoardService,
              private dialog: MatDialog,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      mergeMap(params =>
        this.taskBoardService.getProjectById(params.get('projectId'))
      ),
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.project = res;
    });

    this.taskBoardService.todoTasks$.pipe(takeUntil(this.destroy$)).subscribe((tasks => this.todoTasks = tasks));
    this.taskBoardService.inprogressTasks$.pipe(takeUntil(this.destroy$)).subscribe((tasks => this.inprogressTasks = tasks));
    this.taskBoardService.testTasks$.pipe(takeUntil(this.destroy$)).subscribe((tasks => this.testTasks = tasks));
    this.taskBoardService.doneTasks$.pipe(takeUntil(this.destroy$)).subscribe((tasks => this.doneTasks = tasks));
  }

  drop(event: CdkDragDrop<Task[]>, newTaskStatusType: TaskStatusType) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }

    event.container.data[event.currentIndex].status = newTaskStatusType;

    this.taskBoardService.updateBoard({
      projectId: this.project.id,
      tasks: [
        ...this.todoTasks.map((t, i) => ({ id: t.id, index: i, status: t.status })),
        ...this.inprogressTasks.map((t, i) => ({ id: t.id, index: i, status: t.status })),
        ...this.doneTasks.map((t, i) => ({ id: t.id, index: i, status: t.status })),
      ]
    }).subscribe();
  }


  createTask() {
    this.dialog.open(TaskEditDialogComponent);
  }

}
