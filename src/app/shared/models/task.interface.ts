export interface Task {
  id: string;
  projectId: string;
  index: number;
  title: string;
  description: string;
  status: TaskStatusType;
  complexity: ComplexityType;
  estimatedTime: number; // s
  actualTime: number; // s
}

export type TaskStatusType = 'todo' | 'in_progress' | 'test' | 'done';
export type ComplexityType = 1 | 2 | 3;

export interface UpdateBoardPayload {
  projectId: string;
  tasks: Array<{
    id: string;
    status: TaskStatusType;
    index: number;
  }>;
}
