export interface BurndownChartRealData {
  date: Date;
  value: number; // s
}

export interface BurndownChartDataset {
  data: Array<number>;
  label: string;
}

export type BurndownChartDatasetsType = [BurndownChartDataset, BurndownChartDataset]; // tuple
