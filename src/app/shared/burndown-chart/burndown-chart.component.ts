import { Component, OnInit, Input } from '@angular/core';
import { BurndownChartRealData, BurndownChartDatasetsType } from './shared';
import { TaskBoardService } from 'src/app/task-board/task-board.service';

@Component({
  selector: 'app-burndown-chart',
  templateUrl: './burndown-chart.component.html',
  styleUrls: ['./burndown-chart.component.scss']
})
export class BurndownChartComponent implements OnInit {
  @Input() fullEstimatedTime: number; // s
  @Input() actualTimesData: Array<BurndownChartRealData>;

  chartDatasets: BurndownChartDatasetsType;
  chartLabels: Array<any>;

  readonly chartType = 'line';

  readonly chartColors: Array<any> = [
    {
      backgroundColor: 'transparent',
      borderColor: 'rgba(200, 99, 132, .7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'transparent',
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 2,
    }
  ];

  readonly chartOptions: any = {
    responsive: true
  };

  constructor(private taskBoardService: TaskBoardService) { }

  ngOnInit() {
    this.chartLabels = this.actualTimesData.map(d =>
      d.date.toLocaleDateString('en-US', {
        day: '2-digit',
        month: '2-digit',
      })
    );
    this.initChartDatasets();
  }

  chartClicked(e: any): void { }
  chartHovered(e: any): void { }

  private initChartDatasets() {
    this.chartDatasets = [
      {
        data: this.actualTimesData.map((d, i) =>
          this.taskBoardService.secondsToHours(this.fullEstimatedTime - i * (this.fullEstimatedTime / (this.actualTimesData.length - 1)))),
        label: 'Full estimated time'
      },
      {
        data: this.actualTimesData.map(d => this.taskBoardService.secondsToHours(this.fullEstimatedTime - d.value)),
        label: 'Actual time'
      }
    ];
  }
}
