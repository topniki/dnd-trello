import { NgModule } from '@angular/core';
import { BurndownChartComponent } from './burndown-chart.component';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [BurndownChartComponent],
  imports: [
    ChartsModule,
    WavesModule
  ],
  exports: [BurndownChartComponent]
})
export class BurndownChartModule { }
